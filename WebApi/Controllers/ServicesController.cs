﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace WebApi.Controllers
{
    public class ServicesController : ApiController
    {
        string version = ConfigurationManager.AppSettings["version"];
        string serviceUrl = ConfigurationManager.AppSettings["service.url"];
        HttpClient client = new HttpClient();

        [HttpGet]
        public async Task<Dictionary<string,string>> Get()
        {
            HttpResponseMessage response = await  client.GetAsync(serviceUrl);
            var dic = new Dictionary<string, string>();
            dic.Add("api.message", "REST API " + version + " New Version");

            if (response.IsSuccessStatusCode)
            {
                string message = await  response.Content.ReadAsStringAsync();
                dic.Add("service.message", message.Trim('"'));
            }
            return dic;
        }
        
    }
}
