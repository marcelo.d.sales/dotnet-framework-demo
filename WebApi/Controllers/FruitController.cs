﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;

using System.Web.Http;
using System.Web.Mvc;
using System.Web.UI.WebControls;
using DbAccess;

namespace WebApi.Controllers
{
    public class FruitController : ApiController
    {
        private pocdbEntities Context = new pocdbEntities();
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);        

        [System.Web.Http.HttpGet]
        public IEnumerable<fruits> Get()
        {
            log.Info("call GetFruits");
            using (pocdbEntities db = new pocdbEntities())
            {
                return db.fruits.ToList();
            }
        }

        [System.Web.Http.HttpGet]
        public fruits Get(int id)
        {
            log.Info("call GetFruits by Id "+id);
            using (pocdbEntities db = new pocdbEntities())
            {
                return db.fruits.Find(id);
            }
        }

        [System.Web.Http.HttpPost]
        public IHttpActionResult AddFruit([FromBody] fruits fruits)
        {
            log.Info("call AddFruit");
            if (ModelState.IsValid)
            {
                Context.fruits.Add(fruits);
                Context.SaveChanges();
                return Ok(fruits);
            }
            else
            {
                return BadRequest();
            }
        }

        [System.Web.Http.HttpPut]
        public IHttpActionResult UpdateFruit(int id, [FromBody] fruits fruits)
        {
            log.Info("call UpdateFruit");
            if (ModelState.IsValid) {
                var fruitExists = Context.fruits.Count(e => e.id == id) > 0;
                if (fruitExists)
                {
                    Context.Entry(fruits).State = System.Data.Entity.EntityState.Modified;
                    Context.SaveChanges();
                    return Ok(fruits);
                }
                else
                {
                    return NotFound();
                }
            } else { return BadRequest();}
        }

        [System.Web.Http.HttpDelete]
        public IHttpActionResult DeleteFruit(int id) {
            log.Info("call DeleteFruit for id "+id);
            var fruit = Context.fruits.Find(id);
            if (fruit != null)
            {
                Context.fruits.Remove(fruit);
                Context.SaveChanges();
                return Ok(fruit);
            }
            else
            {
                return NotFound();
            }
        }
    
    }
}
