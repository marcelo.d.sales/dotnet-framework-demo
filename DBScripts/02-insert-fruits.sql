USE [pocdb]
GO
INSERT INTO fruits
	SELECT name from ( values ('Apple')) as data (name)
where not exists (select name from fruits where name = 'Apple');
GO