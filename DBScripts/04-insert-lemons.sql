USE [pocdb]
GO
INSERT INTO fruits
	SELECT name from ( values ('Lemons')) as data (name)
where not exists (select name from fruits where name = 'Lemons');
GO