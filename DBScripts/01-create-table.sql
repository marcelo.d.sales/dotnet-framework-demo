IF NOT EXISTS (SELECT * FROM sysobjects WHERE name='dbo.fruits' and xtype='U')
CREATE TABLE dbo.fruits (
	id int IDENTITY(1,1) NOT NULL,
	name varchar(150) NOT NULL
	);
GO