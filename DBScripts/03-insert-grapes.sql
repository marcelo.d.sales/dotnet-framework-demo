USE [pocdb]
GO
INSERT INTO fruits
	SELECT name from ( values ('Grapes')) as data (name)
where not exists (select name from fruits where name = 'Grapes');
GO