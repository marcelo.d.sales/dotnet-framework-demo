# POC Dotnet framework

### Install Service
```
c:\Windows\Microsoft.NET\Framework\v4.0.30319\InstallUtil.exe C:\Temp\WindowsService.HostAPI\WindowsService_HostAPI.exe
```

### Uninstall
```
c:\Windows\Microsoft.NET\Framework\v4.0.30319\InstallUtil.exe /u C:\Temp\WindowsService.HostAPI\WindowsService_HostAPI.exe
sc delete "WindowsService_HostAPI"
```

glpat-sMk3F3T_s8gxXczC4v1L (GITLAB)

## Server 2019 Install
```
c:\Windows\Microsoft.NET\Framework\v4.0.30319\InstallUtil.exe C:\Deploy\WebAPISelfHosting\WindowsService_HostAPI.exe
```

## Server 2019 Uninstall
```
c:\Windows\Microsoft.NET\Framework\v4.0.30319\InstallUtil.exe /u C:\Deploy\WebAPISelfHosting\WindowsService_HostAPI.exe
sc delete "WebAPISelfHosting"
```
