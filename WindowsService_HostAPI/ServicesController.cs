﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;

namespace WindowsService_HostAPI
{
    public class ServicesController : ApiController
    {
        public String GetMessage()
        {
            string version = ConfigurationManager.AppSettings["version"];
            return "This message comes from Windows Service version " + version + " for POC";
        }
    }
}
