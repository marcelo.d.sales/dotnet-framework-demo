﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;
using System.Timers;
using System.Web.Http;
using System.Web.Http.SelfHost;

namespace WindowsService_HostAPI
{
    public partial class SelfHostService : ServiceBase
    {
        string version = ConfigurationManager.AppSettings["version"];
        int port = int.Parse(ConfigurationManager.AppSettings["port"]);
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public SelfHostService()
        {
            InitializeComponent();
        }

        protected override void OnStart(string[] args)
        {
            log.Info("Service version " + version + " has been started");
            var config = new HttpSelfHostConfiguration("http://localhost:"+port);
            config.Routes.MapHttpRoute(name: "API", routeTemplate: "{controller}/{action}");
            HttpSelfHostServer server = new HttpSelfHostServer(config);
            server.OpenAsync().Wait();
            log.Info("Self Hosting HTTP Service has been started");
        }        

        protected override void OnStop()
        {
            log.Info("Service version " + version + " has been stopped");
        }
        
    }
}
