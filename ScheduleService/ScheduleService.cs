﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;
using System.Timers;

namespace ScheduleService
{
    public partial class ScheduleService : ServiceBase
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        int Interval = int.Parse(ConfigurationManager.AppSettings["interval"]);
        string version = ConfigurationManager.AppSettings["version"];

        Timer timer = new Timer();

        public ScheduleService()
        {
            InitializeComponent();
        }

        protected override void OnStart(string[] args)
        {
            timer.Elapsed += new ElapsedEventHandler(OnElapsedTime);
            timer.Interval = Interval;
            timer.Enabled = true;
            log.Info("Schedule service started succsessfully");
        }

        private void OnElapsedTime(object source, ElapsedEventArgs e)
        {
           log.Info("Time Event fired. [" + version + "]");
        }

        protected override void OnStop()
        {
            timer.Stop();
        }
    }
}
